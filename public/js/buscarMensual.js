function fetchData() {
    const mes = $('#selectMes').val();
    const ano = $('#selectAno').val();
    const url = `/searchMensual?mes=${mes}&ano=${ano}`;

    $.ajax({
        type:'GET',
        url: url,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        data:{
            mes:mes,
            ano:ano
        },
        success:function(data){
            $('#contenido').html(data);

            if ( $.fn.dataTable.isDataTable( '#tablaMensual' ) ) {
                $('#tablaMensual').DataTable();
            }else{
                $('#tablaMensual').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'excel'
                    ],
                    "order": false,
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Ver _MENU_",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "_START_ al _END_ de  _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "pageLength": 100,
                    "bDestroy": true
                });
            }
        }
    });
}

$(function () {
    fetchData();

    $('#selectMes').change(fetchData);
    $('#selectAno').change(fetchData);
});