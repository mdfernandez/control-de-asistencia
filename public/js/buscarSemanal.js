function fetchData() {

    let value = $("#weeklyDatePicker").val();
    let firstDate = moment(value, "DD-MM-YYYY").day(0).format("YYYY-MM-DD");
    let lastDate = moment(value, "DD-MM-YYYY").day(6).format("YYYY-MM-DD");

    const url = `/searchSemanal`;

    $.ajax({
        type:'GET',
        url: url,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        data:{
            firstDate:firstDate,
            lastDate:lastDate
        },
        success:function(data){
            $('#contenido').html(data);

            if ( $.fn.dataTable.isDataTable( '#tablaSemanal' ) ) {
                $('#tablaSemanal').DataTable();
            }else{
                $('#tablaSemanal').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'excel'
                    ],
                    "order": false,
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Ver _MENU_",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "_START_ al _END_ de  _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "pageLength": 100,
                    "bDestroy": true
                });
            }
        }
    });
}

$(function () {

    fetchData();

    $('#weeklyDatePicker').change(fetchData);
});