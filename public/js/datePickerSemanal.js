$(document).ready(function() {

    jQuery('.mydatepicker').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: 'bottom left'
    });


    moment.locale('en', {
        week: {
            dow: 1
        } // Monday is the first day of the week
    });

});