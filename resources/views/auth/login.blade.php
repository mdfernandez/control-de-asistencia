@extends('templates.application.master')

@section('css')
    <link href="{{asset('css/jquerysctipttop.css')}}" rel="stylesheet" type="text/css">
    <style>
        .switch label .lever {
            background-color: #5788c2;
        }

        .switch label .lever::after {
            background-color: #1630c2;
        }
    </style>
@endsection

@section('template-custom-js')

    <script src="/vendor/wrappixel/material-pro/4.2.1/material/js/custom.min.js"></script>


@endsection

@section('layout-content')

    <div id="main-wrapper">
        <section id="wrapper" class="login-register login-sidebar" style="background-image:url({{asset('images/background.jpg')}});">
            <div class="login-box card">
                <div class="card-body" style="overflow-x: hidden; overflow-y:auto;">
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            @include('templates.application.components.navbar-lang')
                        </div>
                    </div>
                    <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login.post') }}" autocomplete="off">
                        @csrf
                        <a href="javascript:void(0)" class="text-center db">
                            {{--<img src="{{asset('images/logo.png')}}" alt="Home" style="height: 10em"/>--}}
                            <h1 class="mt-5 font-weight-bold">{{ _i('Control de Asistencia') }}</h1>
                            <br/>
                        </a>
                        <div class="form-group mt-20">
                            @if(Session::has('error'))
                                <div class="alert alert-danger text-center">
                                    {{Session::get('error')}}
                                </div>
                            @endif
                            @if(Session::has('status'))
                                <div class="alert alert-success text-center">
                                    {{Session::get('status')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <div class="switch">
                                    <label>ENTRADA
                                        <input type="checkbox" name="accion" id="accion">
                                        <span class="lever switch-col-red"></span>SALIDA
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{--<input id="pin" type="tel" placeholder="{{ _i('PIN') }}" class="form-control @error('pin') is-invalid @enderror" name="pin" value="{{ old('pin') }}" required autofocus maxlength="4">--}}

                                <label for="cryxpad-input-field">PIN:</label>
                                <input type="password" class="form-control" id="pin" name="pin" aria-describedby="pin-help" placeholder="Escriba su PIN" maxlength="4">

                                <div class="row mt-4">
                                    <div class="col-sm-12">
                                        <div class="cryxpad-container"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="button"
                                        id="btnRegistrar"> {{ _i('REGISTRAR') }}</button>
                            </div>
                            <div class="col-md-6">
                                <button id="cryxpad-remove-btn" type="button" class="btn btn-lg btn-block text-uppercase waves-effect waves-light btn-danger"> Borrar</button>
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<div class="col-md-12 text-center">--}}
                        {{--   <div class="checkbox checkbox-primary pull-left p-t-0">--}}
                        {{--<input id="checkbox-signup" type="checkbox" {{ old('remember') ? 'checked' : '' }}>--}}
                        {{--<label for="checkbox-signup"> {{ _i('Remember Me') }} </label>--}}
                        {{--</div> --}}
                        {{--<a href="javascript:void(0)" id="to-recover" class="text-dark">--}}
                        {{--<i class="fa fa-lock m-r-5"></i>--}}
                        {{--{{ _i('¿Olvidaste tu contraseña?') }}--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                    </form>
                    {{--<form class="form-horizontal" id="recoverform" method="POST" action="{{ route('password.email') }}">--}}
                    {{--@csrf--}}
                    {{--<div class="form-group ">--}}
                    {{--<div class="col-xs-12">--}}
                    {{--<h3>{{ _i('Recuperar Contraseña') }}</h3>--}}
                    {{--<p class="text-muted">{{ _i('Ingresa tu correo y te enviaremos las instrucciones') }}</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group ">--}}
                    {{--<div class="col-xs-12">--}}
                    {{--<input id="emailRecover" type="email" placeholder="{{ _i('Email') }}" class="form-control" name="email" required>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group text-center m-t-20">--}}
                    {{--<div class="col-xs-12">--}}
                    {{--<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"--}}
                    {{--type="submit"> {{ _i('Enviar enlace') }}</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<a href="javascript:void(0)" id="back-to-login" class="text-dark pull-right">--}}
                    {{--<i class="fa fa-backward m-r-5"></i>--}}
                    {{--{{ _i('Volver al inicio') }}--}}
                    {{--</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</form>--}}

                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.6.1/dist/sweetalert2.all.min.js"></script>
    @if(session('info'))
        <?php $resp = json_decode(session('info')); ?>
        @if(isset($resp->olvido))
            <script>
                Swal.fire({
                    icon: 'error',
                    title: '<h1>{{ $resp->usuario }}</h1><h1 class="text-danger">ACCIÓN NO PERMITIDA</h1>',
                    html: '<h2>Esta intentando registrar nuevamente su <b>{{strtoupper($resp->movimiento)}}</b></h2>',
                    showConfirmButton: true
                })
            </script>
        @else
            @if($resp->tipo=='E')
                <script>
                    Swal.fire({
                        icon: 'success',
                        title: '<h1>{{ $resp->usuario }}</h1><h3>Su <b>ENTRADA</b> ha sido registrada a las:</h3>',
                        html: '<h2 class="display-1"> {{ $resp->hora }} </h2>',
                        showConfirmButton: true
                    })
                </script>
            @else
                @if($resp->tipo=='S')
                    <script>
                        Swal.fire({
                            icon: 'warning',
                            title: '<h1>{{ $resp->usuario }}</h1><h3>Su <b>SALIDA</b> ha sido registrada a las:</h3>',
                            html: '<h2 class="display-1"> {{ $resp->hora }} </h2>',
                            showConfirmButton: true
                        })
                    </script>
                @endif
            @endif
        @endif
    @endif


    <script src="{{asset('js/jquery.cryxpad.js')}}"></script>
    <script type="text/javascript">

        $(function () {
            //Appel par défaut du plug-in
            $('.cryxpad-container').cryxpad({
                'inputFormId': 'pin',
                'removeButtonId': 'cryxpad-remove-btn',
                'validateButtonId': 'cryxpad-validate-btn',
                'carreaux': 5, // nombre de carreaux sur une ligne du clavier
                'width': 50, // longeur d'un bouton
                'height': 50, // hauteur d'un bouton
                /*'buttonClass':"btn btn-primary",*/
            });
        });

        $(function () {
            let accion = localStorage.getItem("accion");
            if (accion == 'Salida') {
                $("#accion").attr("checked", true);
            } else {
                $("#accion").attr("checked", false);
            }

            $('#pin').val('');
        });

        $('#btnRegistrar').click(function () {

            if ($('#accion').prop('checked')) {
                localStorage.setItem("accion", 'Salida');
            } else {
                localStorage.setItem("accion", 'Entrada');
            }

            $('#loginform').submit();
        });

    </script>
@endsection