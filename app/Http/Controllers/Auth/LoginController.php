<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Asistencia;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;
use Xinax\LaravelGettext\Facades\LaravelGettext;
use SweetAlert;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'changeLang']);
    }

    /**
     * Changes the current language and returns to previous page
     * @return Redirect
     */
    public function changeLang($locale = null)
    {
        LaravelGettext::setLocale($locale);
        return back();
    }

    public function login(Request $request)
    {

        $user = User::where('pin', md5($request->pin))->first();

        if (isset($user->id)) {

//            $asistioHoy = Asistencia::where('user_id', $user->id)
//                ->where('fecha', date("Y-m-d"))
//                ->orderBy('id', 'DESC')
//                ->first();
//
//            if (isset($asistioHoy->fecha)) {
//                $hora = $asistioHoy->created_at->format('H:m');
//                Session::flash('error', 'Usted salió a las: ' . $hora);
//                return back();

                $ultimaAsistencia = Asistencia::where('user_id', $user->id)
                ->orderBy('id', 'DESC')
                ->first();

                $ultTipo = $ultimaAsistencia['tipo'];

                $tipo = ($request->accion=='on')? 'S' : 'E';

                //if($asistioHoy->tipo=='E'){
                if($tipo!=$ultTipo){

                    $data['user_id'] = $user->id;
                    $data['fecha'] = date('Y-m-d');
                    $data['tipo'] = $tipo;

                    $asistencia = Asistencia::create($data);

                    Session::flush();

                    $hora = $asistencia->created_at->format('H:i');

                    $resp['usuario'] = $user->nombre;
                    $resp['tipo'] = $tipo;
                    $resp['hora'] = $hora;

                }else{
//                    $data['user_id'] = $user->id;
//                    $data['fecha'] = date('Y-m-d');
//                    $data['tipo'] = ($tipo=='E')? 'S' : 'E';
//
//                    $asistencia = Asistencia::create($data);
//
//                    Session::flush();
//
//                    $hora = $asistencia->created_at->format('H:i');
//
//                    $resp['usuario'] = $user->nombre;
//                    $resp['tipo'] = 'E';
//                    $resp['hora'] = $hora;
                    $resp['olvido'] = 1;
                    $resp['usuario'] = $user->nombre;
                    $resp['movimiento'] = ($ultTipo=='E')? 'Entrada' : 'Salida';
                }

                return back()->with('info', json_encode($resp));
//            } else {
//                $data['user_id'] = $user->id;
//                $data['fecha'] = date('Y-m-d');
//                $data['tipo'] = 'E';
//
//                $asistencia = Asistencia::create($data);
//
//                Session::flush();
//
//                $hora = $asistencia->created_at->format('H:i');
//
//                $resp['usuario'] = $user->nombre;
//                $resp['tipo'] = 'E';
//                $resp['hora'] = $hora;
//
//                return back()->with('info', json_encode($resp));
//
//            }

        }

        Session::flash('error', 'Los datos ingresados son incorrectos o el Usuario no Existe.');
        return back();
    }

    public function admin(Request $request)
    {

        $user = User::where('email', $request->email)
            ->where('password', md5($request->password))
            ->first();

        if (isset($user->id)) {
                $roles = $user->perfiles;
                $isAdmin = 0;
                foreach ($roles AS $rol) {
                    if ($rol->role_id == 1) { //El Rol 4 es el de Asistente
                        $isAdmin = 1;
                    }
                }

                if ($isAdmin) {
                    Auth::login($user);
                    return redirect('/dashboard');
                }
        }

        Session::flash('error', 'Los datos ingresados son incorrectos o el Usuario no es Administrador.');
        return back();
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/admin');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function showAdminForm()
    {
        return view('auth.admin');
    }


}
